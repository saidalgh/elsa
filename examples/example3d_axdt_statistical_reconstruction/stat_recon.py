import yaml
import pyelsa as elsa
import pathlib
import logging
import numpy as np
from scipy.spatial.transform import Rotation as R


def binning_dc(data, binning_fac, log_space):  # non log-space indicates flat-field data
    desc = data.getDataDescriptor()
    data_cp = np.array(data).copy()

    if log_space:
        data_cp = np.exp(-data_cp)

    data_cp = data_cp.reshape(data_cp.shape[0] // binning_fac, binning_fac,
                              data_cp.shape[1] // binning_fac, binning_fac,
                              data_cp.shape[2]
                              ).mean(3).mean(1)

    if log_space:
        data_cp = -np.log(data_cp)

    if not log_space:
        data_cp = data_cp.swapaxes(0, 1)

    new_spacing = desc.getSpacingPerDimension() * binning_fac
    new_spacing[-1] = 1
    desc_cp = elsa.VolumeDescriptor([desc.getNumberOfCoefficientsPerDimension()[0] // binning_fac,
                                     desc.getNumberOfCoefficientsPerDimension()[1] // binning_fac,
                                     desc.getNumberOfCoefficientsPerDimension()[2]],
                                    new_spacing
                                    )
    return elsa.DataContainer(desc_cp, data_cp.swapaxes(0, 2).flatten())


class StatRecon:
    def __init__(self, config_loc, output_loc, binning_factor, recon_type, flat_field, N, verbose):
        self.config_loc = pathlib.Path(config_loc)
        self.output_loc = pathlib.Path(output_loc)
        self.verbose = verbose
        self.binning_factor = binning_factor
        self.recon_type = recon_type
        self.flat_field = flat_field
        self.N = N
        self.logger = logging.getLogger(__name__)
        logging.basicConfig(format='[%(asctime)s][%(from)s][%(levelname)s]: %(message)s')
        if self.verbose:
            self.logger.setLevel(logging.INFO)
        else:
            self.logger.setLevel(logging.WARNING)
        self.__parse()

    def reconstruct(self, eps=0.0001, iters=20):
        who_am_i = {"from": "reconstructor"}

        self.logger.info("Constructing XGI_Descriptor...", extra=who_am_i)
        xgi_desc = elsa.XGIDetectorDescriptor(self.edf_data_dd.getNumberOfCoefficientsPerDimension(),
                                              self.edf_data_dd.getSpacingPerDimension(),
                                              self.geos, self.sense_dir, self.is_parallel)

        projector = elsa.JosephsMethodCUDA(self.vol_desc, xgi_desc)

        self.logger.info("Constructing AXDT_Operator...", extra=who_am_i)

        axdt_op = elsa.AXDTOperator(self.vol_desc, xgi_desc, projector,
                                    self.recon_dirs, self.recon_weights,
                                    elsa.AXDTOperator.Symmetry.Even, self.sph_degree)

        self.logger.info("DONE: AXDT_Operator constructed", extra=who_am_i)

        if not self.flat_field:
            if self.recon_type == 'glogd':
                func = elsa.AXDTStatRecon(self.dci_dc,
                                          axdt_op,
                                          elsa.AXDTStatRecon.StatReconType.Gaussian_log_d)
            else:
                assert self.recon_type == 'gd'
                func = elsa.AXDTStatRecon(self.dci_dc,
                                          axdt_op,
                                          elsa.AXDTStatRecon.StatReconType.Gaussian_d)

        else:
            if self.recon_type == 'glogd':
                func = elsa.AXDTStatRecon(-elsa.log(self.b_dc / self.a_dc / (self.ffb_dc / self.ffa_dc)),
                                          axdt_op,
                                          elsa.AXDTStatRecon.StatReconType.Gaussian_log_d)
            elif self.recon_type == 'gd':
                func = elsa.AXDTStatRecon(-elsa.log(self.b_dc / self.a_dc / (self.ffb_dc / self.ffa_dc)),
                                          axdt_op,
                                          elsa.AXDTStatRecon.StatReconType.Gaussian_d)
            elif self.recon_type == 'gb':
                proj_desc = elsa.PlanarDetectorDescriptor(
                    self.edf_data_dd.getNumberOfCoefficientsPerDimension(),
                    self.edf_data_dd.getSpacingPerDimension(),
                    self.geos)
                absorp_op = elsa.JosephsMethodCUDA(self.vol_desc, proj_desc)
                func = elsa.AXDTStatRecon(self.ffa_dc,
                                          self.ffb_dc,
                                          self.a_dc,
                                          self.b_dc,
                                          absorp_op,
                                          axdt_op,
                                          self.N,
                                          elsa.AXDTStatRecon.StatReconType.Gaussian_approximate_racian)
            else:
                assert self.recon_type == 'rb'
                proj_desc = elsa.PlanarDetectorDescriptor(
                    self.edf_data_dd.getNumberOfCoefficientsPerDimension(),
                    self.edf_data_dd.getSpacingPerDimension(),
                    self.geos)
                absorp_op = elsa.JosephsMethodCUDA(self.vol_desc, proj_desc)
                func = elsa.AXDTStatRecon(self.ffa_dc,
                                          self.ffb_dc,
                                          self.a_dc,
                                          self.b_dc,
                                          absorp_op,
                                          axdt_op,
                                          self.N,
                                          elsa.AXDTStatRecon.StatReconType.Racian_direct)

        x_desc = func.getDomainDescriptor()
        x0 = elsa.DataContainer(x_desc)
        x0 *= 0

        solver = None  # Choose an appropriate solver
        # solver = elsa.CGNL_Custom(func, eps, eps)

        self.logger.info("Solving the problem...", extra=who_am_i)
        result = solver.solve(iters, x0)

        self.logger.info("Writing the reconstruction result to recon_result.edf...", extra=who_am_i)
        self.output_loc.mkdir(parents=True, exist_ok=True)

        elsa.EDF.write(result.getBlock(1), str(self.output_loc.joinpath("recon_result_dci.edf").absolute()))

        if self.recon_type in ['gb', 'rb']:
            elsa.EDF.write(result.getBlock(0), str(self.output_loc.joinpath("recon_result_amp.edf").absolute()))

    def __parse(self):
        who_am_i = {"from": "parser"}

        with open(self.config_loc.absolute(), "r") as config_stream:
            config = yaml.safe_load(config_stream)
        self.logger.info("Config file location: " + str(self.config_loc.absolute()), extra=who_am_i)
        self.logger.info("Got following config contents: " + str(config), extra=who_am_i)

        if not self.flat_field:
            dci_loc = self.config_loc.parent.joinpath(config["data"]["dci-file"])
            dci_neglog = config["data"]["dci-neglog-applied"]
            assert (dci_neglog is True)
        else:
            a_loc = self.config_loc.parent.joinpath(config["data"]["a-file"])
            b_loc = self.config_loc.parent.joinpath(config["data"]["b-file"])
            ffa_loc = self.config_loc.parent.joinpath(config["data"]["ffa-file"])
            ffb_loc = self.config_loc.parent.joinpath(config["data"]["ffb-file"])
            a_neglog = config["data"]["a-neglog-applied"]
            b_neglog = config["data"]["b-neglog-applied"]
            ffa_neglog = config["data"]["ffa-neglog-applied"]
            ffb_neglog = config["data"]["ffb-neglog-applied"]
            assert (not a_neglog and not b_neglog and not ffa_neglog and not ffb_neglog)

        if config["projection"]["type"] == "parallel":
            self.is_parallel = True
            dst_src_center = 1000000.0
            dst_detec_center = 1.0
        else:
            assert (config["projection"]["type"] == "projective")
            self.is_parallel = False
            dst_src_center = config["projection"]["source-center"]
            dst_detec_center = config["projection"]["detector-center"]
        self.logger.info("Parallel beams: " + str(self.is_parallel), extra=who_am_i)
        angle_loc = self.config_loc.parent.joinpath(config["projection"]["angles-file"])
        self.sense_dir = config["projection"]["sensDir"]
        self.logger.info("Sensitivity direction: " + str(self.sense_dir), extra=who_am_i)

        binning_fac = self.binning_factor

        vol_sz = config["reconstruction"]["size"]
        for i in range(len(vol_sz)):
            vol_sz[i] //= binning_fac
        self.logger.info("Reconstruction volume size: " + str(vol_sz), extra=who_am_i)

        vol_spacing = config["reconstruction"]["spacing"]
        for i in range(len(vol_spacing)):
            vol_spacing[i] *= binning_fac
        self.logger.info("Reconstruction spacing: " + str(vol_spacing), extra=who_am_i)

        self.vol_desc = elsa.VolumeDescriptor(vol_sz, vol_spacing)

        self.sph_degree = int(config["reconstruction"]["sphMaxDegree"])
        self.logger.info("Spherical harmonics max degree: " + str(self.sph_degree), extra=who_am_i)

        mode = config["reconstruction"]["mode"]
        assert (mode == "SphericalHarmonics")

        dir_loc = self.config_loc.parent.joinpath(config["reconstruction"]["dirsfile"])

        if not self.flat_field:
            dci_dc = elsa.EDF.readf(str(dci_loc.absolute()))
            self.dci_dc = binning_dc(dci_dc, binning_fac, True)
            del dci_dc
            self.edf_data_dd = self.dci_dc.getDataDescriptor()
        else:
            a_dc = elsa.EDF.readf(str(a_loc.absolute()))
            self.a_dc = binning_dc(a_dc, binning_fac, False)
            del a_dc
            b_dc = elsa.EDF.readf(str(b_loc.absolute()))
            self.b_dc = binning_dc(b_dc, binning_fac, False)
            del b_dc
            ffa_dc = elsa.EDF.readf(str(ffa_loc.absolute()))
            self.ffa_dc = binning_dc(ffa_dc, binning_fac, False)
            del ffa_dc
            ffb_dc = elsa.EDF.readf(str(ffb_loc.absolute()))
            self.ffb_dc = binning_dc(ffb_dc, binning_fac, False)
            del ffb_dc
            self.edf_data_dd = self.a_dc.getDataDescriptor()

        self.logger.info(
            "edf data size: " + str(self.edf_data_dd.getNumberOfCoefficientsPerDimension()),
            extra=who_am_i)

        recon_dirs_weights_comb = np.loadtxt(dir_loc, comments="#", delimiter=" ")
        assert (recon_dirs_weights_comb.shape[1] == 4)
        self.logger.info("Reconstruction direction-weight matrix size: " + str(recon_dirs_weights_comb.shape),
                         extra=who_am_i)
        self.recon_dirs = recon_dirs_weights_comb[:, :3].copy()
        self.recon_weights = recon_dirs_weights_comb[:, -1:].reshape(-1).copy()

        indexed_angles = np.loadtxt(angle_loc, comments="#", delimiter=" ")
        assert (indexed_angles.shape[0] == self.edf_data_dd.getNumberOfCoefficientsPerDimension()[2])
        assert (indexed_angles.shape[1] == 4)
        self.logger.info("Geometry matrix size: " + str(indexed_angles.shape), extra=who_am_i)
        index_expected = np.arange(1, indexed_angles.shape[0] + 1)
        assert (np.all(index_expected == indexed_angles[:, 0].reshape(-1)))

        angles = indexed_angles[:, [2, 1, 3]].copy()
        self.geos = []

        for i in range(angles.shape[0]):
            rot_mat = R.from_euler("YZY", angles[i], degrees=True).as_matrix()
            self.geos.append(elsa.Geometry(dst_src_center, dst_detec_center,
                                           self.vol_desc,
                                           self.edf_data_dd,
                                           rot_mat
                                           )
                             )
