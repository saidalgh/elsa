
#include <optional>

#include "Functional.h"
#include "Solver.h"
#include "LinearOperator.h"
#include "StrongTypes.h"
#include "MaybeUninitialized.hpp"
#include "ProximalOperator.h"

namespace elsa
{
    /**
     * @brief Proximal Optimized Gradient Method (POGM)
     *
     * POGM minimizes function of the the same for as PGD. See the documentation there.
     *
     * The update steps for POGM are:
     * @f[
     * 	  \begin{split}
     *        \theta_k &= \begin{cases}%
     *		      \frac{1}{2}(1 + \sqrt{4 \theta_{k-1}^2 + 1}) & \text{ if } 2 \leq k < N \\%
     *		      \frac{1}{2}(1 + \sqrt{8 \theta_{k-1}^2 + 1}) & \text{ if } k = N        %
     *	      \end{cases} \\%
     *	      \gamma_k &= \frac{1}{L} \frac{2\theta_{k-1} + \theta_k - 1}{\theta_k} \\%
     *	      \omega_k &= x_{k+1} - \frac{1}{L} \nabla f(x_{k-1}) \\%
     *	      z_k &= \omega_k +%
     *	          \underset{Nesterov}{\underbrace{\frac{\theta_{k-1} - 1}{\theta_k}(\omega_k +
     *\omega_{k-1})}}+% \underset{OGM}{\underbrace{\frac{\theta_{k-1}}{\theta_k}(\omega_k -
     *x_{k-1})}}+% \underset{POGM}{\underbrace{\frac{\theta_{k-1} - 1}{L
     *\gamma_{k-1}\theta_k}(z_{k-1} - x_{k-1})}}\\% x_k &= \operatorname{prox}_{\gamma_k g}(z_k)
     * \end{split}
     * @f]
     *
     * References:
     * - https://arxiv.org/abs/1512.07516
     *
     * @see For a more detailed discussion of the type of problem for this solver,
     * see PGD.
     *
     * @author
     * - David Frank - Initial implementation
     *
     * @tparam data_t data type for the domain and range of the problem, defaulting to real_t
     */
    template <typename data_t = real_t>
    class POGM : public Solver<data_t>
    {
    public:
        /// Scalar alias
        using Scalar = typename Solver<data_t>::Scalar;

        /// Construct POGM with a least squares data fidelity term
        ///
        /// @note The step length for least squares can be chosen to be dependend on the Lipschitz
        /// constant. Compute it using `powerIterations(adjoint(A) * A)`. Depending on which
        /// literature, both \f$ \frac{2}{L} \f$ and \f$ \frac{1}{L}\f$. If mu is not given, the
        /// step length is chosen by default, the computation of the power method might be
        /// expensive.
        ///
        /// @param A the operator for the least squares data term
        /// @param b the measured data for the least squares data term
        /// @param g prox-friendly function
        /// @param mu the step length
        POGM(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
             const Functional<data_t>& h, std::optional<data_t> mu = std::nullopt,
             data_t epsilon = std::numeric_limits<data_t>::epsilon());

        /// Construct POGM with a weighted least squares data fidelity term
        ///
        /// @note The step length for least squares can be chosen to be dependend on the Lipschitz
        /// constant. Compute it using `powerIterations(adjoint(A) * A)`. Depending on which
        /// literature, both \f$ \frac{2}{L} \f$ and \f$ \frac{1}{L}\f$. If mu is not given, the
        /// step length is chosen by default, the computation of the power method might be
        /// expensive.
        ///
        /// @param A the operator for the least squares data term
        /// @param b the measured data for the least squares data term
        /// @param W the weights (usually `counts / I0`)
        /// @param prox the proximal operator for g
        /// @param mu the step length
        POGM(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
             const DataContainer<data_t>& W, const Functional<data_t>& h,
             std::optional<data_t> mu = std::nullopt,
             data_t epsilon = std::numeric_limits<data_t>::epsilon());

        /// Construct POGM with a given data fidelity term
        POGM(const Functional<data_t>& g, const Functional<data_t>& h, data_t mu,
             data_t epsilon = std::numeric_limits<data_t>::epsilon());

        /// default destructor
        ~POGM() override = default;

        /**
         * @brief Solve the optimization problem, i.e. apply iterations number of iterations of
         * POGM
         *
         * @param[in] iterations number of iterations to execute
         *
         * @returns the approximated solution
         */
        DataContainer<data_t>
            solve(index_t iterations,
                  std::optional<DataContainer<data_t>> x0 = std::nullopt) override;

    protected:
        /// implement the polymorphic clone operation
        auto cloneImpl() const -> POGM<data_t>* override;

        /// implement the polymorphic comparison operation
        auto isEqual(const Solver<data_t>& other) const -> bool override;

    private:
        /// The LASSO optimization problem
        std::unique_ptr<Functional<data_t>> g_;

        std::unique_ptr<Functional<data_t>> h_;

        // ProximalOperator<data_t> prox_;

        /// the step size
        data_t mu_;

        /// variable affecting the stopping condition
        data_t epsilon_;
    };
} // namespace elsa
