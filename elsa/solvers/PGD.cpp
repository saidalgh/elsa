#include "PGD.h"
#include "DataContainer.h"
#include "Functional.h"
#include "LeastSquares.h"
#include "LinearOperator.h"
#include "LinearResidual.h"
#include "ProximalL1.h"
#include "Solver.h"
#include "TypeCasts.hpp"
#include "Logger.h"
#include "PowerIterations.h"

#include "WeightedLeastSquares.h"
#include "spdlog/stopwatch.h"

namespace elsa
{
    template <typename data_t>
    PGD<data_t>::PGD(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                     const Functional<data_t>& h, std::optional<data_t> mu, data_t epsilon)
        : g_(LeastSquares<data_t>(A, b).clone()), h_(h.clone()), mu_(0), epsilon_(epsilon)
    {
        if (!h.isProxFriendly()) {
            throw Error("PGD: h must be prox friendly");
        }

        if (mu.has_value()) {
            mu_ = *mu;
        } else {
            Logger::get("PGD")->info("Computing Lipschitz constant for least squares...");
            // Chose it a little larger, to be safe
            auto L = 1.05 * powerIterations(adjoint(A) * A);
            mu_ = 1 / L;
            Logger::get("PGD")->info("Step length chosen to be: {}", mu_);
        }
    }

    template <typename data_t>
    PGD<data_t>::PGD(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                     const DataContainer<data_t>& W, const Functional<data_t>& h,
                     std::optional<data_t> mu, data_t epsilon)
        : g_(WeightedLeastSquares<data_t>(A, b, W).clone()),
          h_(h.clone()),
          mu_(0),
          epsilon_(epsilon)
    {
        if (!h.isProxFriendly()) {
            throw Error("APGD: h must be prox friendly");
        }

        if (mu.has_value()) {
            mu_ = *mu;
        } else {
            Logger::get("PGD")->info("Computing Lipschitz constant for least squares...");
            // Chose it a little larger, to be safe
            auto L = 1.05 * powerIterations(adjoint(A) * A);
            mu_ = 1 / L;
            Logger::get("PGD")->info("Step length chosen to be: {}", mu_);
        }
    }

    template <typename data_t>
    PGD<data_t>::PGD(const Functional<data_t>& g, const Functional<data_t>& h, data_t mu,
                     data_t epsilon)
        : g_(g.clone()), h_(h.clone()), mu_(mu), epsilon_(epsilon)
    {
        if (!h.isProxFriendly()) {
            throw Error("PGD: h must be prox friendly");
        }

        if (!g.isDifferentiable()) {
            throw Error("PGD: g must be differentiable");
        }
    }

    template <typename data_t>
    auto PGD<data_t>::solve(index_t iterations, std::optional<DataContainer<data_t>> x0)
        -> DataContainer<data_t>
    {
        spdlog::stopwatch aggregate_time;

        auto x = extract_or(x0, g_->getDomainDescriptor());
        auto grad = emptylike(x);
        auto y = emptylike(x);

        Logger::get("PGD")->info("| {:^6} | {:^12} | {:^12} | {:^9} |", "iter", "g", "gradient",
                                 "elapsed");

        for (index_t iter = 0; iter < iterations; ++iter) {
            g_->getGradient(x, grad);

            // y = x - mu_ * grad
            lincomb(1, x, -mu_, grad, y);

            // apply proximal
            x = h_->proximal(y, mu_);

            if (grad.squaredL2Norm() <= epsilon_) {
                Logger::get("PGD")->info("SUCCESS: Reached convergence at {}/{} iteration",
                                         iter + 1, iterations);
                return x;
            }

            Logger::get("PGD")->info("| {:>6} | {:>12.3} | {:>12.3} | {:>8.3}s |", iter,
                                     g_->evaluate(x), grad.squaredL2Norm(), aggregate_time);
        }

        Logger::get("PGD")->warn("Failed to reach convergence at {} iterations", iterations);

        return x;
    }

    template <typename data_t>
    auto PGD<data_t>::cloneImpl() const -> PGD<data_t>*
    {
        return new PGD<data_t>(*g_, *h_, mu_, epsilon_);
    }

    template <typename data_t>
    auto PGD<data_t>::isEqual(const Solver<data_t>& other) const -> bool
    {
        auto otherPGD = downcast_safe<PGD>(&other);
        if (!otherPGD)
            return false;

        if (std::abs(mu_ - otherPGD->mu_) > 1e-5)
            return false;

        if (epsilon_ != otherPGD->epsilon_)
            return false;

        return true;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class PGD<float>;
    template class PGD<double>;
} // namespace elsa
