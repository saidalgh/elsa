#include "LBFGS.h"
#include "Logger.h"

namespace elsa
{

    template <typename data_t>
    LBFGS<data_t>::LBFGS(const Functional<data_t>& problem,
                         const LineSearchMethod<data_t>& line_search_method, const index_t& memory,
                         const data_t& tol)
        : Solver<data_t>(),
          _problem(problem.clone()),
          _ls(line_search_method.clone()),
          _tol{tol},
          _m{memory}
    {
        // sanity check
        if (tol < 0)
            throw InvalidArgumentError("LBFGS: tolerance has to be non-negative");
        if (memory < 1)
            throw InvalidArgumentError("LBFGS: memory has to be positive");
    }

    template <typename data_t>
    DataContainer<data_t> LBFGS<data_t>::solve(index_t iterations,
                                               std::optional<DataContainer<data_t>> x0)
    {

        std::vector<DataContainer<data_t>> siVec;
        std::vector<DataContainer<data_t>> yiVec;
        std::vector<data_t> rhoVec(_m, 1);
        std::vector<data_t> alphaVec(_m, 1);

        siVec.reserve(_m);
        yiVec.reserve(_m);

        auto xi = extract_or(x0, _problem->getDomainDescriptor());
        auto xi_1 = DataContainer<data_t>(_problem->getDomainDescriptor());
        auto gi = _problem->getGradient(xi);
        auto gi_1 = DataContainer<data_t>(_problem->getDomainDescriptor());

        auto di = -gi;
        for (index_t i = 0; i < iterations; ++i) {
            Logger::get("LBFGS")->info("iteration {} of {}", i + 1, iterations);

            xi_1 = xi;
            gi_1 = gi;
            xi += _ls->solve(xi, di) * di;
            gi = _problem->getGradient(xi);

            if (gi.l2Norm() < _tol) {
                return xi;
            }

            if (i < _m) {
                siVec.push_back(xi - xi_1);
                yiVec.push_back(gi - gi_1);
            } else {
                siVec[i % _m] = xi - xi_1;
                yiVec[i % _m] = gi - gi_1;
            }
            rhoVec[i % _m] = 1 / yiVec[i % _m].dot(siVec[i % _m]);
            di = gi;
            for (index_t j = i % _m, k = 0; k < i + 1 && k < _m; ++k, j = (j - 1 + _m) % _m) {
                alphaVec[j] = rhoVec[j] * siVec[j].dot(di);
                di -= alphaVec[j] * yiVec[j];
            }

            auto gamma = yiVec[i % _m].dot(siVec[i % _m]) / yiVec[i % _m].dot(yiVec[i % _m]);
            di *= gamma;

            for (index_t k = 0, j = (i < _m ? 0 : (i + 1) % _m); k < i + 1 && k < _m;
                 ++k, j = (j + 1) % _m) {
                auto beta = rhoVec[j] * yiVec[j].dot(di);
                di += siVec[j] * (alphaVec[j] - beta);
            }

            di = -di;
        }

        return xi;
    } // namespace elsa

    template <typename data_t>
    LBFGS<data_t>* LBFGS<data_t>::cloneImpl() const
    {
        return new LBFGS(*_problem, *_ls, _m, _tol);
    }

    template <typename data_t>
    bool LBFGS<data_t>::isEqual(const Solver<data_t>& other) const
    {
        auto otherLBFGS = downcast_safe<LBFGS<data_t>>(&other);
        if (!otherLBFGS)
            return false;

        // TODO: compare line search methods
        return _tol == otherLBFGS->_tol && _m == otherLBFGS->_m;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class LBFGS<float>;
    template class LBFGS<double>;
} // namespace elsa
