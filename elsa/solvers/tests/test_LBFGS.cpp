/**
 * @file test_LBFGS.cpp
 *
 * @brief Tests for the LBFGS class
 *
 * @author Said Alghabra - initial code
 */

#include "MatrixOperator.h"
#include "doctest/doctest.h"

#include "LBFGS.h"
#include "Identity.h"
#include "LinearResidual.h"
#include "LeastSquares.h"
#include "L2Squared.h"
#include "Logger.h"
#include "Scaling.h"
#include "VolumeDescriptor.h"
#include "StrongWolfeCondition.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("solvers");

TYPE_TO_STRING(LBFGS<float>);
TYPE_TO_STRING(LBFGS<double>);

template <template <typename> typename T, typename data_t>
constexpr data_t return_data_t(const T<data_t>&);

TEST_CASE_TEMPLATE("LBFGS: Solving a simple linear problem", data_t, float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);

    GIVEN("a linear problem")
    {
        Matrix_t<data_t> mat({{0, 1, 2, 3}, {4, 5, 6, 7}, {8, 9, 10, 11}});
        MatrixOperator<data_t> A(mat);

        VolumeDescriptor desc({{3}});
        Vector_t<data_t> v({{0, 1, 2}});
        DataContainer<data_t> b(desc, v);
        LeastSquares<data_t> prob(A, b);

        WHEN("setting up an L-BFGS solver with StrongWolfeCondition")
        {
            StrongWolfeCondition<data_t> line_search{prob, 10, 1e-4, 0.9};
            LBFGS<data_t> solver{prob, line_search, 10};

            THEN("the clone works correctly")
            {
                auto gdClone = solver.clone();

                CHECK_NE(gdClone.get(), &solver);
                CHECK_EQ(*gdClone, solver);

                AND_THEN("it works as expected")
                {
                    auto solution = solver.solve(11);

                    CHECK_EQ(solution[0], doctest::Approx(0.175));
                    CHECK_EQ(solution[1], doctest::Approx(0.1));
                    CHECK_EQ(solution[2], doctest::Approx(0.025));
                    CHECK_EQ(solution[3], doctest::Approx(-0.05));
                }
            }
        }
    }
}

TEST_CASE_TEMPLATE("LBFGS: Solving a Tikhonov problem", data_t, float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);

    GIVEN("a Tikhonov problem")
    {
        Matrix_t<data_t> mat({{0, 1, 2, 3}, {4, 5, 6, 7}, {8, 9, 10, 11}});
        MatrixOperator<data_t> A(mat);

        VolumeDescriptor desc({{3}});
        Vector_t<data_t> v({{0, 1, 2}});
        DataContainer<data_t> b(desc, v);
        LeastSquares<data_t> fn(A, b);

        // the regularization term
        L2Squared<data_t> l2(A.getDomainDescriptor());
        auto prob = fn + l2;

        WHEN("setting up an L-BFGS solver with StrongWolfeCondition")
        {
            StrongWolfeCondition<data_t> line_search{prob, 10, 1e-4, 0.9};
            LBFGS<data_t> solver{prob, line_search};

            THEN("the clone works correctly")
            {
                auto gdClone = solver.clone();

                CHECK_NE(gdClone.get(), &solver);
                CHECK_EQ(*gdClone, solver);
            }
            THEN("it works as expected")
            {
                auto solution = solver.solve(11);

                CHECK_EQ(solution[0], doctest::Approx(0.14668315));
                CHECK_EQ(solution[1], doctest::Approx(0.08858673));
                CHECK_EQ(solution[2], doctest::Approx(0.03049032));
                CHECK_EQ(solution[3], doctest::Approx(-0.0276061));
            }
        }
    }
}

TEST_SUITE_END();
