#include "DataContainer.h"
#include "doctest/doctest.h"

#include "CGLS.h"
#include "Logger.h"
#include "Scaling.h"
#include "VolumeDescriptor.h"
#include "Identity.h"
#include "TypeCasts.hpp"
#include "elsaDefines.h"
#include "testHelpers.h"
#include "MatrixOperator.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("solvers");

template <template <typename> typename T, typename data_t>
constexpr data_t return_data_t(const T<data_t>&);

TEST_CASE_TEMPLATE("CGLS: Solving A 3x4 test problem ", data_t, float, double)
{
    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);
    srand((unsigned int) 666);

    Matrix_t<data_t> mat({{0, 1, 2, 3}, {4, 5, 6, 7}, {8, 9, 10, 11}});
    MatrixOperator<data_t> A(mat);

    VolumeDescriptor desc({{3}});
    Vector_t<data_t> v({{0, 1, 2}});
    DataContainer<data_t> b(desc, v);

    WHEN("Solving A x = b")
    {
        CGLS<data_t> cgls(A, b, 0);

        auto x = cgls.solve(3);

        CHECK_EQ(x[0], doctest::Approx(0.175));
        CHECK_EQ(x[1], doctest::Approx(0.1));
        CHECK_EQ(x[2], doctest::Approx(0.025));
        CHECK_EQ(x[3], doctest::Approx(-0.05));

        THEN("A clone is equal to the original and produces the same result")
        {
            auto clone = cgls.clone();

            CHECK_EQ(*clone, cgls);

            auto xclone = clone->solve(3);

            CHECK_EQ(xclone[0], doctest::Approx(0.175));
            CHECK_EQ(xclone[1], doctest::Approx(0.1));
            CHECK_EQ(xclone[2], doctest::Approx(0.025));
            CHECK_EQ(xclone[3], doctest::Approx(-0.05));
        }
    }

    WHEN("Solving [A (damp*I)]^T x = [b 0] (Tikhonov)")
    {
        CGLS<data_t> cgls(A, b, 1);

        auto x = cgls.solve(3);

        CHECK_EQ(x[0], doctest::Approx(0.14668315));
        CHECK_EQ(x[1], doctest::Approx(0.08858673));
        CHECK_EQ(x[2], doctest::Approx(0.03049032));
        CHECK_EQ(x[3], doctest::Approx(-0.0276061));

        THEN("A clone is equal to the original and produces the same result")
        {
            auto clone = cgls.clone();

            CHECK_EQ(*clone, cgls);

            auto xclone = clone->solve(3);

            CHECK_EQ(xclone[0], doctest::Approx(0.14668315));
            CHECK_EQ(xclone[1], doctest::Approx(0.08858673));
            CHECK_EQ(xclone[2], doctest::Approx(0.03049032));
            CHECK_EQ(xclone[3], doctest::Approx(-0.0276061));
        }
    }
}

TEST_CASE_TEMPLATE("CGLS: Solving A 10x15 test problem ", data_t, float, double)
{
    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);
    srand((unsigned int) 666);

    // clang-format off
    Matrix_t<data_t> mat(
        {{3.41367722e-01, 9.23280171e-01, 1.84060994e-01, 1.89084598e-01, 9.26883862e-01, 1.66669398e-01, 8.12401636e-01, 7.44006189e-01, 2.97490436e-01, 2.01112142e-01, 3.70471221e-01, 2.41600063e-01, 4.38929367e-01, 8.99000036e-01, 1.86407654e-01},
         {1.57020860e-01, 6.05902396e-01, 4.51240099e-02, 4.81955196e-01, 8.65522263e-01, 2.67539936e-01, 2.92080480e-01, 5.01157213e-01, 6.82204687e-01, 4.30609293e-01, 6.98417256e-01, 7.35339913e-01, 8.70093333e-01, 1.85462224e-01, 9.91890745e-01},
         {7.56127891e-01, 6.19617107e-03, 6.82409193e-01, 9.68180644e-01, 7.03328009e-01, 8.94648522e-01, 6.63118966e-04, 2.08737932e-01, 3.42666560e-01, 7.68255214e-01, 8.10011179e-01, 1.88358793e-01, 1.16224590e-02, 7.57195033e-01, 1.12017290e-01},
         {4.69052735e-01, 1.79946888e-02, 4.86111673e-02, 3.07895687e-01, 8.38028837e-01, 2.76260700e-01, 4.48627638e-01, 8.94868437e-02, 5.58527304e-01, 4.86955832e-01, 1.30328173e-01, 6.93902864e-01, 4.22956281e-01, 8.08537597e-01, 9.76061621e-01},
         {9.77463771e-01, 8.89338721e-01, 6.90225435e-01, 4.85495238e-01, 1.06087847e-01, 7.53466167e-01, 1.55963613e-01, 8.09493223e-01, 1.29667895e-01, 9.40394536e-03, 3.75435190e-01, 9.27345837e-01, 2.07972296e-01, 1.08992606e-01, 5.68445262e-01},
         {7.14528116e-01, 2.53362746e-01, 4.21225996e-01, 6.86219660e-02, 9.67050385e-01, 7.44709926e-01, 1.73004036e-01, 4.32790323e-01, 8.30108357e-01, 2.20805274e-01, 3.84958902e-01, 7.13142077e-01, 4.26548855e-01, 3.56728690e-01, 9.91957469e-01},
         {9.06861239e-01, 9.80192658e-01, 7.38791243e-01, 7.62527984e-01, 7.24162529e-01, 8.44628063e-01, 7.84580666e-01, 4.04611408e-01, 1.11233848e-01, 4.07423054e-01, 2.66278754e-01, 2.31399756e-01, 3.99649043e-01, 2.32897930e-02, 3.57675081e-02},
         {8.43198789e-01, 7.79881419e-01, 5.77199744e-01, 3.09326734e-01, 3.14370092e-01, 4.68656054e-01, 8.31334454e-01, 3.70238671e-01, 1.69131152e-01, 7.47531543e-01, 1.51313913e-01, 7.69295822e-02, 3.41224526e-01, 5.61915149e-02, 4.45349791e-02},
         {3.82845115e-01, 8.01251125e-01, 6.34965362e-01, 9.29344600e-01, 1.41833354e-01, 3.92032173e-01, 6.56055814e-01, 3.13588873e-01, 8.92479399e-01, 3.76555175e-01, 1.12752887e-01, 1.71772631e-01, 9.15433012e-01, 9.22156461e-01, 2.27597517e-01},
         {2.47128037e-01, 1.08955937e-01, 6.11287870e-01, 4.09150022e-01, 8.98994568e-01, 1.31900958e-01, 8.90635061e-01, 5.17747471e-01, 4.13140510e-01, 4.77487673e-02, 8.73758281e-01, 6.41636919e-02, 7.49490761e-02, 5.34332993e-01, 8.84241628e-01}});
    // clang-format on
    MatrixOperator<data_t> A(mat);

    VolumeDescriptor desc({{10}});
    Vector_t<data_t> v({{0.99562922, 0.99933787, 0.7542503, 0.81625623, 0.00126698, 0.33784856,
                         0.68858776, 0.00629371, 0.94325065, 0.60112315}});
    DataContainer<data_t> b(desc, v);

    WHEN("Solving A x = b")
    {
        CGLS<data_t> cgls(A, b, 0);

        auto x = cgls.solve(20);

        Vector_t<data_t> expected({{-0.37252303, 0.17977196, -0.329151, 0.74044361, 0.66352396,
                                    0.04772574, 0.04944286, -0.22203567, -0.15906877, -0.20356914,
                                    0.00497961, 0.13894406, 0.30616053, 0.35763347, -0.12654501}});

        CAPTURE(expected.transpose());
        CAPTURE(x);

        for (int i = 0; i < expected.size(); ++i) {
            CHECK_EQ(x[i], doctest::Approx(expected[i]).epsilon(0.01));
        }

        THEN("A clone is equal to the original and produces the same result")
        {
            auto clone = cgls.clone();

            CHECK_EQ(*clone, cgls);

            auto xclone = clone->solve(20);

            for (int i = 0; i < expected.size(); ++i) {
                CHECK_EQ(xclone[i], doctest::Approx(expected[i]).epsilon(0.01));
            }
        }
    }

    WHEN("Solving [A (damp*I)]^T x = [b 0] with damp = 1 (Tikhonov problem)")
    {
        CGLS<data_t> cgls(A, b, 1);

        auto x = cgls.solve(20);

        Vector_t<data_t> expected({{-0.13916626, 0.08860243, -0.09237181, 0.25098282, 0.27841333,
                                    -0.01973573, 0.08502288, -0.00359706, 0.10577225, 0.07581986,
                                    0.10930453, 0.00946965, 0.20163608, 0.28820802, 0.00377833}});

        THEN("A clone is equal to the original and produces the same result")
        {
            auto clone = cgls.clone();

            CHECK_EQ(*clone, cgls);

            auto xclone = clone->solve(20);

            for (int i = 0; i < expected.size(); ++i) {
                CHECK_EQ(xclone[i], doctest::Approx(expected[i]).epsilon(0.01));
            }
        }
    }
}

TEST_SUITE_END();
