#include "doctest/doctest.h"

#include "Error.h"
#include "PGD.h"
#include "Identity.h"
#include "Logger.h"
#include "VolumeDescriptor.h"
#include "ConstantFunctional.h"
#include "IndicatorFunctionals.h"
#include "L1Norm.h"
#include "testHelpers.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("solvers");

TEST_CASE_TEMPLATE("PDG: Solving a least squares problem", data_t, float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);

    VolumeDescriptor volDescr({4, 7});

    Vector_t<data_t> bVec(volDescr.getNumberOfCoefficients());
    bVec.setRandom();
    DataContainer<data_t> b(volDescr, bVec);

    Identity<data_t> A(volDescr);

    PGD<data_t> solver(A, b, ZeroFunctional<data_t>{volDescr}, 1);

    auto reco = solver.solve(50);

    CHECK_EQ(reco.squaredL2Norm(), doctest::Approx(b.squaredL2Norm()).epsilon(0.001));

    THEN("Clone is equal to original one")
    {
        auto clone = solver.clone();

        CHECK_EQ(*clone, solver);
        CHECK_NE(clone.get(), &solver);
    }
}

TEST_CASE_TEMPLATE("PDG: Solving a weighted least squares problem", data_t, float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);

    VolumeDescriptor volDescr({4, 7});

    Vector_t<data_t> bVec(volDescr.getNumberOfCoefficients());
    bVec.setRandom();
    DataContainer<data_t> b(volDescr, bVec);

    Vector_t<data_t> wVec(volDescr.getNumberOfCoefficients());
    wVec.setOnes();
    DataContainer<data_t> W(volDescr, wVec);

    Identity<data_t> A(volDescr);

    PGD<data_t> solver(A, b, W, ZeroFunctional<data_t>{volDescr}, 1);

    auto reco = solver.solve(50);

    CHECK_EQ(reco.squaredL2Norm(), doctest::Approx(b.squaredL2Norm()).epsilon(0.001));

    THEN("Clone is equal to original one")
    {
        auto clone = solver.clone();

        CHECK_EQ(*clone, solver);
        CHECK_NE(clone.get(), &solver);
    }
}

TEST_CASE_TEMPLATE("PDG: Solving a least squares problem with non negativity constraint", data_t,
                   float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);

    VolumeDescriptor volDescr({4, 7});

    // Ensure data is actually non negative
    Vector_t<data_t> bVec(volDescr.getNumberOfCoefficients());
    bVec = bVec.setRandom().cwiseAbs();
    DataContainer<data_t> b(volDescr, bVec);

    Identity<data_t> A(volDescr);

    auto g = IndicatorNonNegativity<data_t>{volDescr};
    PGD<data_t> solver(A, b, g, 1);

    auto reco = solver.solve(50);

    CHECK_EQ(reco.squaredL2Norm(), doctest::Approx(b.squaredL2Norm()).epsilon(0.001));

    THEN("Clone is equal to original one")
    {
        auto clone = solver.clone();

        CHECK_EQ(*clone, solver);
        CHECK_NE(clone.get(), &solver);
    }
}

TEST_SUITE_END();
