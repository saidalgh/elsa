#pragma once

#include <optional>

#include "DataContainer.h"
#include "LinearOperator.h"
#include "Solver.h"
#include "StrongTypes.h"
#include "ProximalOperator.h"

namespace elsa
{
    /**
     * Aссelerated Linearized Bregman converges faster than Linearized Bregman and solves problems
     * of form:
     * @f[
     * \min_{x} \frac{1}{2\mu} || A x - b ||_2^2 + |x|
     * @f]
     *
     * by iteratively solving:
     *
     * @f[
     * \begin{aligned}
     * x^{k+1} & =\mu \cdot \operatorname{prox}\left(\tilde{v}^k, 1\right) \\
     * v^{k+1} & =\tilde{v}^k+\beta A^T\left(b-A x^{k+1}\right) \\
     * \tilde{v}^{k+1} & :=\alpha_k v^{k+1}+\left(1-\alpha_k\right) v^k
     * \end{aligned}
     * @f]
     *
     * where @f$ a_k = \frac{2k + 3}{k + 3} @f$
     *
     * References:
     * - https://arxiv.org/pdf/1106.5413.pdf
     */
    template <typename data_t = real_t>
    class ALB : public Solver<data_t>
    {
    public:
        /// Construct Linearized Bregman
        ALB(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
            const ProximalOperator<data_t> prox, data_t mu = 5,
            std::optional<data_t> beta = std::nullopt, data_t epsilon = 1e-5);

        /// make copy constructor deletion explicit
        ALB(const ALB<data_t>&) = delete;

        /// default destructor
        ~ALB() override = default;

        DataContainer<data_t>
            solve(index_t iterations,
                  std::optional<DataContainer<data_t>> x0 = std::nullopt) override;

    protected:
        /// implement the polymorphic clone operation
        auto cloneImpl() const -> ALB<data_t>* override;

        /// implement the polymorphic comparison operation
        auto isEqual(const Solver<data_t>& other) const -> bool override;

    private:
        /// The LASSO optimization problem
        std::unique_ptr<LinearOperator<data_t>> A_;

        DataContainer<data_t> b_;

        ProximalOperator<data_t> prox_;

        /// wage of fidelity term
        data_t mu_;

        /// step size
        data_t beta_;

        /// variable affecting the stopping condition
        data_t epsilon_;
    };
} // namespace elsa
