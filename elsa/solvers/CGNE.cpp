#include "CGNE.h"
#include "DataContainer.h"
#include "Error.h"
#include "LinearOperator.h"
#include "LinearResidual.h"
#include "Solver.h"
#include "TypeCasts.hpp"
#include "spdlog/stopwatch.h"
#include "Logger.h"

namespace elsa
{
    template <class data_t>
    CGNE<data_t>::CGNE(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                       SelfType_t<data_t> tol)
        : A_(A.clone()),
          AtA_((adjoint(*A_) * (*A_)).clone()),
          b_(b),
          Atb_(A_->applyAdjoint(b_)),
          r_(empty<data_t>(AtA_->getRangeDescriptor())),
          c_(empty<data_t>(AtA_->getRangeDescriptor())),
          Ac_(empty<data_t>(AtA_->getRangeDescriptor())),
          kold_(100000),
          tol_(tol)
    {
        this->name_ = "CGNE";
    }

    template <typename data_t>
    DataContainer<data_t> CGNE<data_t>::setup(std::optional<DataContainer<data_t>> x0)
    {
        auto x = extract_or(x0, A_->getDomainDescriptor());

        // Residual b - A(x)
        r_ = AtA_->apply(x);
        r_ *= data_t{-1};
        r_ += Atb_;

        c_.assign(r_);

        Ac_ = empty<data_t>(AtA_->getRangeDescriptor());

        // Squared Norm of residual
        kold_ = r_.squaredL2Norm();

        // setup done!
        this->configured_ = true;

        return x;
    }

    template <typename data_t>
    DataContainer<data_t> CGNE<data_t>::step(DataContainer<data_t> x)
    {
        AtA_->apply(c_, Ac_);
        auto cAc = c_.dot(Ac_);

        alpha_ = kold_ / cAc;

        // Update x and residual
        x += alpha_ * c_;
        r_ -= alpha_ * Ac_;

        auto k = r_.squaredL2Norm();
        beta_ = k / kold_;

        // c = r + beta * c
        lincomb(1, r_, beta_, c_, c_);

        // store k for next iteration
        kold_ = k;

        return x;
    }

    template <typename data_t>
    bool CGNE<data_t>::shouldStop() const
    {
        return r_.l2Norm() < tol_;
    }

    template <typename data_t>
    std::string CGNE<data_t>::formatHeader() const
    {
        return fmt::format("{:^15} | {:^15} | {:^15} | {:^15}", "r", "c", "alpha", "beta");
    }

    template <typename data_t>
    std::string CGNE<data_t>::formatStep(const DataContainer<data_t>& x) const
    {
        return fmt::format("{:>15.10} | {:>15.10} | {:>15.10} | {:>15.10}", r_.l2Norm(),
                           c_.l2Norm(), alpha_, beta_);
    }

    template <class data_t>
    bool CGNE<data_t>::isEqual(const Solver<data_t>& other) const
    {
        auto cgne = downcast_safe<CGNE>(&other);
        return cgne && *cgne->A_ == *A_ && cgne->b_ == b_;
    }

    template <class data_t>
    CGNE<data_t>* CGNE<data_t>::cloneImpl() const
    {
        return new CGNE(*A_, b_, tol_);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class CGNE<float>;
    template class CGNE<double>;

} // namespace elsa
