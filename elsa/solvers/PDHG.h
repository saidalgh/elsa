#pragma once

#include "Functional.h"
#include "Solver.h"
#include "LinearOperator.h"
#include "ProximalOperator.h"
#include "DataContainer.h"
#include "elsaDefines.h"
#include <optional>

namespace elsa
{
    /**
     * @brief A class implementing the Primal Dual Hybrid Gradient reconstruction
     * algorithm.
     *
     * The algorithms solves the problem of the form:
     * @f[
     * \min_x f(x) + g(Kx)
     * @f]
     * There @f$f@f$ needs to be prox friendly, and @f$g@f$ needs a prox friendly
     * convex conjugate. The update steps are given as
     * - @f$ y_{n+1} = \operatorname{prox}_{\sigma g^*}(y_n + \sigma K \bar{x}_n})@f$ the gradient
     * update step for the dual problem
     * - @f$ x_{n+1} = \operatorname{prox}_{\tau f}(x_n + \tau K^* y_{n+1}})@f$ the gradient update
     * step for the primal problem
     * - @f$ \bar{x}_{n+1} = x_{n+1} + \theta (x_{n+1} + x_n)@f$ an over-relaxation step of the
     * primal variable
     *
     * The convergence is guaranteed if @f$\theta = 1@f$, the operator norm @f$||K||@f$,
     * the dual step size @f$\sigma@f$ and the and the primal step size @f$\tau@f$ satisfy:
     * @f[
     * \sigma \tau ||K|| < 1
     * @f]
     *
     * Default common step sizes are:
     * - @f$\sigma = \frac{1}{||K||}@f$ and @f$\tau = \frac{1}{||K||}@f$
     * - given @f$\sigma@f$ and @f$\tau = \frac{1}{\sigma ||K||^2}@f$
     * - or given @f$\tau@f$ and @f$\sigma = \frac{1}{\tau ||K||^2}@f$
     * The first option is chosen if no step sizes are provided.
     *
     * References:
     * - Antonin Chambolle and Thomas Pock, “A First-Order Primal-Dual Algorithm for Convex Problems
     * with Applications to Imaging,” Journal of Mathematical Imaging and Vision 40, no. 1 (May 1,
     * 2011): 120–45, https://doi.org/10.1007/s10851-010-0251-1.
     * - Emil Y. Sidky, Jakob H. Jørgensen, and Xiaochuan Pan, “Convex Optimization Problem
     * Prototyping for Image Reconstruction in Computed Tomography with the Chambolle-Pock
     * Algorithm,” Physics in Medicine and Biology 57, no. 10 (May 21, 2012): 3065–91,
     * https://doi.org/10.1088/0031-9155/57/10/3065.
     */
    template <class data_t>
    class PDHG final : public Solver<data_t>
    {
    public:
        PDHG(const LinearOperator<data_t>& K, const Functional<data_t>& f,
             const Functional<data_t>& g, std::optional<data_t> sigma = std::nullopt,
             std::optional<data_t> tau = std::nullopt, std::optional<data_t> normK = std::nullopt,
             SelfType_t<data_t> theta = 1);

        /// default destructor
        ~PDHG() override = default;

        DataContainer<data_t>
            solve(index_t iterations,
                  std::optional<DataContainer<data_t>> x0 = std::nullopt) override;

        PDHG<data_t>* cloneImpl() const override;

        bool isEqual(const Solver<data_t>& other) const override;

    private:
        /// The stacked linear operator \f$K\f$
        std::unique_ptr<LinearOperator<data_t>> K_;

        /// The \f$f\f$
        std::unique_ptr<Functional<data_t>> f_;

        /// The proximal operator for \f$g\f$
        std::unique_ptr<Functional<data_t>> g_;

        /// Step length for the dual
        data_t sigma_;

        /// Step length for the primal
        data_t tau_;

        /// Relaxation parameter
        data_t theta_;
    };
} // namespace elsa
