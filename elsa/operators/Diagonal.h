#pragma once

#include "DataDescriptor.h"
#include "LinearOperator.h"

namespace elsa
{
    /**
     * @brief Operator representing the block diagonal operator.
     *
     * This class represents a class of the form:
     * @f[
     * A = \begin{bmatrix}
     * A_1 &  & \\
     * & \ddots &  \\
     * &  & A_n
     * \end{bmatrix}
     * @f]
     * where @f$n@f$ operators are used in the diagonal of the operator.
     *
     * @tparam data_t data type for the domain and range of the operator, defaulting to real_t
     */
    template <typename data_t = real_t>
    class Diagonal : public LinearOperator<data_t>
    {
    public:
        using OperatorList = typename std::vector<std::unique_ptr<LinearOperator<data_t>>>;
        /**
         * @brief Constructor for the block diagional operator, specifying the domain (= range).
         *
         * @param[in] descriptor DataDescriptor describing the domain and range of the operator
         */
        Diagonal(const DataDescriptor& domain, const DataDescriptor& range,
                 const OperatorList& ops);

        /// default destructor
        ~Diagonal() override = default;

    protected:
        /// default copy constructor, hidden from non-derived classes to prevent potential slicing
        Diagonal(const Diagonal<data_t>&) = default;

        /**
         * @brief apply the block diagional operator
         *
         * @param[in] x input DataContainer (in the domain of the operator)
         * @param[out] Ax output DataContainer (in the range of the operator)
         */
        void applyImpl(const DataContainer<data_t>& x, DataContainer<data_t>& Ax) const override;

        /**
         * @brief apply the adjoint of the block diagional operator
         *
         * @param[in] y input DataContainer (in the range of the operator)
         * @param[out] Aty output DataContainer (in the domain of the operator)
         */
        void applyAdjointImpl(const DataContainer<data_t>& y,
                              DataContainer<data_t>& Aty) const override;

        /// implement the polymorphic clone operation
        Diagonal<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LinearOperator<data_t>& other) const override;

    private:
        OperatorList ops_;
    };
} // namespace elsa
