#include "DataContainer.h"
#include "MatrixOperator.h"
#include "doctest/doctest.h"
#include "Identity.h"
#include "VolumeDescriptor.h"
#include "elsaDefines.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("core");

TEST_CASE_TEMPLATE("MatrixOperator: ", data_t, float, double)
{
    Matrix_t<data_t> mat({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {10, 11, 12}});

    MatrixOperator<data_t> A(mat);
    auto clone = A.clone();

    THEN("Clone is equal to original")
    {
        CHECK_EQ(A, *clone);
    }

    CHECK_EQ(A.getDomainDescriptor().getNumberOfCoefficients(), mat.cols());
    CHECK_EQ(A.getRangeDescriptor().getNumberOfCoefficients(), mat.rows());
    CHECK_EQ(clone->getDomainDescriptor().getNumberOfCoefficients(), mat.cols());
    CHECK_EQ(clone->getRangeDescriptor().getNumberOfCoefficients(), mat.rows());

    THEN("Check apply works as expected")
    {
        Vector_t<data_t> v({{1, 1, 1}});
        DataContainer<data_t> x(VolumeDescriptor({{3}}), v);

        auto Ax = A.apply(x);
        CHECK_EQ(Ax.getSize(), mat.rows());
        CHECK_EQ(Ax[0], mat(0, 0) + mat(0, 1) + mat(0, 2));
        CHECK_EQ(Ax[1], mat(1, 0) + mat(1, 1) + mat(1, 2));
        CHECK_EQ(Ax[2], mat(2, 0) + mat(2, 1) + mat(2, 2));

        Ax = clone->apply(x);
        CHECK_EQ(Ax.getSize(), mat.rows());
        CHECK_EQ(Ax[0], mat(0, 0) + mat(0, 1) + mat(0, 2));
        CHECK_EQ(Ax[1], mat(1, 0) + mat(1, 1) + mat(1, 2));
        CHECK_EQ(Ax[2], mat(2, 0) + mat(2, 1) + mat(2, 2));
    }

    THEN("Check applyAdjoint works as expected")
    {
        Vector_t<data_t> v({{1, 1, 1, 1}});
        DataContainer<data_t> y(VolumeDescriptor({{4}}), v);

        auto Aty = A.applyAdjoint(y);
        CHECK_EQ(Aty.getSize(), mat.cols());
        CHECK_EQ(Aty[0], mat(0, 0) + mat(1, 0) + mat(2, 0) + mat(3, 0));
        CHECK_EQ(Aty[1], mat(0, 1) + mat(1, 1) + mat(2, 1) + mat(3, 1));
        CHECK_EQ(Aty[2], mat(0, 2) + mat(1, 2) + mat(2, 2) + mat(3, 2));

        Aty = clone->applyAdjoint(y);
        CHECK_EQ(Aty.getSize(), mat.cols());
        CHECK_EQ(Aty[0], mat(0, 0) + mat(1, 0) + mat(2, 0) + mat(3, 0));
        CHECK_EQ(Aty[1], mat(0, 1) + mat(1, 1) + mat(2, 1) + mat(3, 1));
        CHECK_EQ(Aty[2], mat(0, 2) + mat(1, 2) + mat(2, 2) + mat(3, 2));
    }

    THEN("Apply with wrongy sized input doesn't work")
    {
        Vector_t<data_t> v({{1, 1, 1, 1, 1, 1, 1, 1, 1, 1}});
        DataContainer<data_t> x(VolumeDescriptor({{10}}), v);

        CHECK_THROWS_AS(A.apply(x), Error);
    }

    THEN("ApplyAdjoint with wrongy sized input doesn't work")
    {
        Vector_t<data_t> v({{1, 1, 1, 1, 1, 1, 1, 1, 1, 1}});
        DataContainer<data_t> y(VolumeDescriptor({{10}}), v);

        CHECK_THROWS_AS(A.applyAdjoint(y), Error);
    }
}
