#include "doctest/doctest.h"
#include "Diagonal.h"
#include "Scaling.h"
#include "DataContainer.h"
#include "LinearOperator.h"
#include "IdenticalBlocksDescriptor.h"
#include "VolumeDescriptor.h"
#include "testHelpers.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("core");

TEST_CASE_TEMPLATE("Diagonal:", data_t, float, double)
{
    auto single_desc = VolumeDescriptor({{10}});
    auto op1 = Scaling<data_t>(single_desc, 1);
    auto op2 = Scaling<data_t>(single_desc, 2);
    auto op3 = Scaling<data_t>(single_desc, 3);

    auto desc = IdenticalBlocksDescriptor(3, single_desc);
    std::vector<std::unique_ptr<LinearOperator<data_t>>> ops;
    ops.push_back(op1.clone());
    ops.push_back(op2.clone());
    ops.push_back(op3.clone());
    auto diag = Diagonal(desc, desc, ops);

    auto ones = DataContainer<data_t>(desc);
    ones = 1;

    auto res = diag.apply(ones);

    for (int i = 0; i < 10; ++i) {
        CHECK_EQ(res[i], 1);
    }
    for (int i = 10; i < 20; ++i) {
        CHECK_EQ(res[i], 2);
    }
    for (int i = 20; i < 30; ++i) {
        CHECK_EQ(res[i], 3);
    }
}

TEST_SUITE_END();
