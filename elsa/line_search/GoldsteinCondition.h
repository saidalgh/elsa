#pragma once

#include "LineSearchMethod.h"

namespace elsa
{
    /**
     * @brief Goldstein Condition
     *
     * GoldsteinCondition is a line search method attempting to find a step size
     * @f$\alpha@f$ that satisfies the sufficient decrease condition while making sure
     * that the step size @f$\alpha@f$ is not too short using the following inequalities:
     *
     * @f[
     * f(x_i) + (1-c) \alpha \nabla f_i^T d_i \le f(x_i+\alpha d_i) \le f(x_i) + c \alpha \nabla
     * f_i^T d_i
     * @f]
     *
     * where @f$f: \mathbb{R}^n \to \mathbb{R}@f$ is differentiable,
     * @f$\nabla f_i: \mathbb{R}^n is the gradient of f at x_i@f$,
     * @f$ d_i: \mathbb{R}^n is the search direction @f$, and
     * @f$ c: is a constant @f$.
     *
     * References:
     * - See Wright and Nocedal, ‘Numerical Optimization’, 2nd Edition, 2006, pp. 33-36.
     *
     * @author
     * - Said Alghabra - initial code
     *
     * @tparam data_t data type for the domain and range of the problem, defaulting to real_t
     */
    template <typename data_t = real_t>
    class GoldsteinCondition : public LineSearchMethod<data_t>
    {
    public:
        GoldsteinCondition(const Functional<data_t>& problem, data_t amax = 10, data_t c = 0.25,
                           index_t max_iterations = 10);
        ~GoldsteinCondition() override = default;
        data_t solve(DataContainer<data_t> xi, DataContainer<data_t> di) override;

    private:
        // largest allowed step size
        data_t _amax;

        // parameter affecting the sufficient decrease condition
        data_t _c;

        data_t _zoom(data_t a_lo, data_t a_hi, data_t f_lo, data_t f_hi, data_t f0, data_t der_f_lo,
                     data_t der_f0, const DataContainer<data_t>& xi,
                     const DataContainer<data_t>& di, index_t max_iterations = 10);

        /// implement the polymorphic clone operation
        GoldsteinCondition<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LineSearchMethod<data_t>& other) const override;
    };
} // namespace elsa
