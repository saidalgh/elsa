#include "FixedStepSize.h"
namespace elsa
{

    template <typename data_t>
    FixedStepSize<data_t>::FixedStepSize(const Functional<data_t>& problem, data_t step_size)
        : LineSearchMethod<data_t>(problem), _step_size(step_size)
    {
        // sanity checks
        if (step_size <= 0)
            throw InvalidArgumentError("FixedStepSize: step_size has to be greater than 0");
    }

    template <typename data_t>
    data_t FixedStepSize<data_t>::solve(DataContainer<data_t> xi, DataContainer<data_t> di)
    {
        return _step_size;
    }

    template <typename data_t>
    FixedStepSize<data_t>* FixedStepSize<data_t>::cloneImpl() const
    {
        return new FixedStepSize(*this->_problem, _step_size);
    }

    template <typename data_t>
    bool FixedStepSize<data_t>::isEqual(const LineSearchMethod<data_t>& other) const
    {
        auto otherFS = downcast_safe<FixedStepSize<data_t>>(&other);
        if (!otherFS)
            return false;

        return (_step_size == otherFS->_step_size);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class FixedStepSize<float>;
    template class FixedStepSize<double>;
} // namespace elsa
