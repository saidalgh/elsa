#include <doctest/doctest.h>

#include "testHelpers.h"
#include "MixedL21Norm.h"
#include "Identity.h"
#include "VolumeDescriptor.h"
#include "IdenticalBlocksDescriptor.h"
#include "TypeCasts.hpp"

#include <iostream>

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("functionals");

TEST_CASE_TEMPLATE("MixedL21Norm norm", TestType, float, double)
{

    GIVEN("just data")
    {
        IndexVector_t numCoeff(1);
        numCoeff << 4;
        VolumeDescriptor dd(numCoeff);
        IdenticalBlocksDescriptor ibd(3, dd);

        WHEN("instantiating")
        {
            MixedL21Norm<TestType> func(ibd);

            THEN("the functional is as expected")
            {
                REQUIRE_EQ(func.getDomainDescriptor(), ibd);
            }

            THEN("a clone behaves as expected")
            {
                auto l1Clone = func.clone();

                REQUIRE_NE(l1Clone.get(), &func);
                REQUIRE_EQ(*l1Clone, func);
            }

            THEN("the evaluate, gradient and Hessian work as expected")
            {
                Vector_t<TestType> dataVec(ibd.getNumberOfCoefficients());
                dataVec << 1, 2, 3, 4, 10, 11, -3, -8, -1, -5, 12, 13;
                DataContainer<TestType> dc(ibd, dataVec);

                REQUIRE(checkApproxEq(func.evaluate(dc), 50.85460955169532));
                REQUIRE_THROWS_AS(func.getGradient(dc), LogicError);
                REQUIRE_THROWS_AS(func.getHessian(dc), LogicError);
            }
        }
    }
}

TEST_SUITE_END();
