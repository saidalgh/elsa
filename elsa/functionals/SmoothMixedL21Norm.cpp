#include "SmoothMixedL21Norm.h"
#include "BlockDescriptor.h"
#include "BlockLinearOperator.h"
#include "Identity.h"
#include "Scaling.h"
#include "functions/Square.hpp"

using namespace std;

namespace elsa
{
    template <typename data_t>
    SmoothMixedL21Norm<data_t>::SmoothMixedL21Norm(const DataDescriptor& domainDescriptor,
                                                   data_t epsilon)
        : Functional<data_t>(domainDescriptor), epsilon{epsilon}
    {
    }

    template <typename data_t>
    data_t SmoothMixedL21Norm<data_t>::evaluateImpl(const DataContainer<data_t>& Rx) const
    {
        ;
        return Rx.l21SmoothMixedNorm(epsilon);
    }

    template <typename data_t>
    void SmoothMixedL21Norm<data_t>::getGradientImpl(const DataContainer<data_t>& Rx,
                                                     DataContainer<data_t>& out) const
    {

        auto tmp = DataContainer<data_t>(Rx.getBlock(0).getDataDescriptor());
        tmp = 0;

        for (index_t i = 0; i < Rx.getNumberOfBlocks(); ++i) {
            tmp += (square(Rx.getBlock(i)));
        }

        tmp += (epsilon * epsilon);
        tmp = sqrt(tmp);

        for (index_t i = 0; i < Rx.getNumberOfBlocks(); ++i) {
            out.getBlock(i) = Rx.getBlock(i) / tmp;
        }
    }
    template <typename data_t>
    LinearOperator<data_t>
        SmoothMixedL21Norm<data_t>::getHessianImpl(const DataContainer<data_t>& Rx) const
    {

        throw LogicError("SmoothMixedL21Norm: not differentiable, so no hessian! (busted!)");
    }
    template <typename data_t>
    SmoothMixedL21Norm<data_t>* SmoothMixedL21Norm<data_t>::cloneImpl() const
    {
        return new SmoothMixedL21Norm(this->getDomainDescriptor(), epsilon);
    }
    template <typename data_t>
    bool SmoothMixedL21Norm<data_t>::isEqual(const Functional<data_t>& other) const
    {
        if (!Functional<data_t>::isEqual(other))
            return false;

        return is<SmoothMixedL21Norm>(other);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class SmoothMixedL21Norm<float>;
    template class SmoothMixedL21Norm<double>;
    template class SmoothMixedL21Norm<complex<float>>;
    template class SmoothMixedL21Norm<complex<double>>;

} // namespace elsa
