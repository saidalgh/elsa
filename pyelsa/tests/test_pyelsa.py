import pyelsa as elsa

import numpy as np

import unittest
import random
import math
import os


class PyelsaTest(unittest.TestCase):
    def assert_equal(self, arr: np.array, dc: elsa.DataContainer, fail_msg: str = None):
        "Assert whether arr and dc represent the same array"

        arr_f = arr.flatten(order="C")
        for i in range(len(arr_f)):
            self.assertEqual(arr_f[i], dc[i], fail_msg)

    def test_convert_to_and_from_numpy_1d(self):
        """Convert a 1D numpy array to and from a DataContainer"""
        x = np.arange(10, dtype=np.float32)
        dc = elsa.DataContainer(x)

        for i in range(10):
            self.assertEqual(x[i], dc[i])

        y = np.asarray(dc)

        for i in range(10):
            self.assertEqual(y[i], dc[i])

        np.testing.assert_array_equal(x, dc)
        np.testing.assert_array_equal(x, y)
        np.testing.assert_array_equal(dc, y)

        np.testing.assert_array_equal(x.shape, dc.shape)
        np.testing.assert_array_equal(x.strides, dc.strides)
        np.testing.assert_array_equal(x.itemsize, dc.itemsize)
        np.testing.assert_array_equal(x.ndim, dc.ndim)

    def test_convert_to_and_from_numpy_2d(self):
        """Convert a 1D numpy array to and from a DataContainer"""
        x = np.arange(10, dtype=np.float32).reshape(2, 5)
        dc = elsa.DataContainer(x)

        for i in range(2):
            for j in range(2):
                self.assertEqual(x[i, j], dc[i, j])

        y = np.asarray(dc)

        for i in range(2):
            for j in range(2):
                self.assertEqual(y[i, j], dc[i, j])

        np.testing.assert_array_equal(x, dc)
        np.testing.assert_array_equal(x, y)
        np.testing.assert_array_equal(dc, y)

        np.testing.assert_array_equal(x.ndim, dc.ndim)

    def test_convert_to_and_from_numpy_3d(self):
        """Convert a 3D numpy array to and from a DataContainer"""
        x = np.arange(70, dtype=np.float32).reshape(2, 5, 7)
        dc = elsa.DataContainer(x)

        for i in range(2):
            for j in range(5):
                for k in range(7):
                    self.assertEqual(x[i, j, k], dc[i, j, k])

        y = np.asarray(dc)

        for i in range(2):
            for j in range(5):
                for k in range(7):
                    self.assertEqual(y[i, j, k], dc[i, j, k])

        np.testing.assert_array_equal(x, dc)
        np.testing.assert_array_equal(x, y)
        np.testing.assert_array_equal(dc, y)

        np.testing.assert_array_equal(x.ndim, dc.ndim)

    def test_memory_layout(self):
        "Test memory layout of DataContainer and converted np.array"

        # test C-array -> DataContainer conversion
        x = np.arange(6, dtype=np.float32).reshape(3, 2)
        dc = elsa.DataContainer(x)
        np.testing.assert_array_equal(
            x,
            dc,
            "Transferring C-ordered array to elsa results in the same array"
        )

        # test Fortran-array -> DataContainer conversion
        x = np.array([[1, 2, 3], [4, 5, 6]], dtype=np.float32, order="F")
        dc = elsa.DataContainer(x)
        np.testing.assert_array_equal(
            x,
            dc,
            "Transferring Fortran-ordered array to elsa results in the same array"
        )

    def test_create_view_from_dc(self):
        """Testing creationg a view from an elsa DataContainer to NumPy"""
        x = np.arange(6, dtype=np.float32).reshape(3, 2)
        desc = elsa.VolumeDescriptor([2, 3])
        dc = elsa.DataContainer(x, desc)

        view = np.array(dc, copy=False)

        np.testing.assert_array_equal(
            view,
            dc,
            "Creating a view from an elsa DataContainer to NumPy works",
        )

        view[1, 0] = random.randint(1, 1000)
        self.assertEqual(
            view[1, 0],
            dc[1, 0],
            "Changes in the NumPy object are reflected to the elsa side",
        )

    def test_linear_traversal(self):
        """Test basic linear traversal of DataContainer"""
        x = np.arange(6).reshape(3, 2)
        desc = elsa.VolumeDescriptor([2, 3])
        dc = elsa.DataContainer(x, desc)

        for i in range(dc.size):
            self.assertEqual(
                dc[i], dc[np.flip(desc.getCoordinateFromIndex(i))])

        np.testing.assert_array_equal(x, dc)

    def create_deep_copy_from_dc(self):
        x = np.arange(6).reshape(3, 2)
        desc = elsa.VolumeDescriptor([2, 3])
        dc = elsa.DataContainer(x, desc)

        # test np.array copy of DataContainer
        copy = np.array(dc, copy=True)
        np.testing.assert_array_equal(copy,  dc)

    def test_datacontainer(self):
        size = 1000
        v = np.random.randn(size).astype(np.float32)
        w = np.random.randn(size).astype(np.float32)

        desc = elsa.VolumeDescriptor([size])

        x = elsa.DataContainer(desc, v)
        y = elsa.DataContainer(desc, w)

        np.testing.assert_allclose(+x, +v, rtol=1e-5)
        np.testing.assert_allclose(-x, -v, rtol=1e-5)

        np.testing.assert_allclose(x + y, v + w, rtol=1e-4)
        np.testing.assert_allclose(x - y, v - w, rtol=1e-4)
        np.testing.assert_allclose(x * y, v * w, rtol=1e-4)
        np.testing.assert_allclose(x / y, v / w, rtol=1e-4)

        np.testing.assert_allclose(x + 5, v + 5, rtol=1e-4)
        np.testing.assert_allclose(x - 5, v - 5, rtol=1e-4)
        np.testing.assert_allclose(x * 5, v * 5, rtol=1e-4)
        np.testing.assert_allclose(x / 5, v / 5, rtol=1e-4)

        np.testing.assert_allclose(5 + y, 5 + w, rtol=1e-4)
        np.testing.assert_allclose(5 - y, 5 - w, rtol=1e-4)
        np.testing.assert_allclose(5 * y, 5 * w, rtol=1e-4)
        np.testing.assert_allclose(5 / y, 5 / w, rtol=1e-4)

        x += y
        v += w
        np.testing.assert_allclose(x, v, rtol=1e-4)
        x -= y
        v -= w
        np.testing.assert_allclose(x, v, rtol=1e-4)
        x *= y
        v *= w
        np.testing.assert_allclose(x, v, rtol=1e-4)
        x /= y
        v /= w
        np.testing.assert_allclose(x, v, rtol=1e-4)

        for i in range(0, size):
            self.assertAlmostEqual(x[i], v[i], 5)
            self.assertAlmostEqual(y[i], w[i], 5)

        x[0] = 12345
        self.assertAlmostEqual(x[0], 12345)

    def _test_reconstruction(
        self,
        size,
        phantom,
        trajectory_generator,
        *args,
        iterCount=20,
        error=0.004,
        **kwargs
    ):
        "Try performing a simple reconstruction"

        numAngles = 50
        arc = 360

        volDesc = phantom.getDataDescriptor()

        # generate circular trajectory
        sinoDesc = trajectory_generator.createTrajectory(
            numAngles, volDesc, arc, size * 100.0, size, *args, **kwargs
        )

        # setup operator for X-ray transform
        projector = elsa.SiddonsMethod(volDesc, sinoDesc)

        # simulate sinogram
        sino = projector.apply(phantom)

        # solve the reconstruction problem
        solver = elsa.CGLS(projector, sino)
        recon = solver.solve(iterCount)

        # compute mse and check that it is within bounds
        mse = (recon - phantom).squaredL2Norm() / size**2
        self.assertLess(
            mse,
            error,
            "Mean squared error of reconstruction too large",
        )

    def _test_reconstruction_2d(self, *args, **kwargs):
        size = 16.0
        # generating a 2d phantom
        phantom = elsa.phantoms.modifiedSheppLogan([size, size])
        self._test_reconstruction(size, phantom, *args, **kwargs)

    def _test_reconstruction_3d(self, *args, **kwargs):
        size = 16.0
        # generating a 3d phantom
        phantom = elsa.phantoms.modifiedSheppLogan([size, size, size])
        self._test_reconstruction(size, phantom, *args, **kwargs)

    def test_reconstruction_2d_curved_more_iterations(self):
        self._test_reconstruction_2d(
            elsa.CurvedCircleTrajectoryGenerator, elsa.Radian(2.0), iterCount=40
        )

    def test_reconstruction_2d_curved_smaller_angle(self):
        self._test_reconstruction_2d(
            elsa.CurvedCircleTrajectoryGenerator, elsa.Radian(0.5)
        )

    def test_reconstruction_2d_planar(self):
        self._test_reconstruction_2d(elsa.CircleTrajectoryGenerator)

    def test_reconstruction_3d_curved_more_iterations(self):
        self._test_reconstruction_3d(
            elsa.CurvedCircleTrajectoryGenerator,
            elsa.Radian(2.0),
            iterCount=40,
            error=0.2,
        )

    def test_reconstruction_3d_curved_smaller_angle(self):
        self._test_reconstruction_3d(
            elsa.CurvedCircleTrajectoryGenerator, elsa.Radian(0.5), error=0.2
        )

    def test_reconstruction_3d_planar(self):
        self._test_reconstruction_3d(elsa.CircleTrajectoryGenerator, error=0.2)

    # def test_logging(self):
    #     "Test logging module interface"
    #
    #     # the elsa.Logger appears to bypass the Python-side sys.stdout
    #     # so we only test file logging
    #     logfile_name = "test_pyelsa_log.txt"
    #     if os.path.exists(logfile_name):
    #         os.remove(logfile_name)
    #     elsa.Logger.enableFileLogging(logfile_name)
    #
    #     elsa.Logger.setLevel(elsa.LogLevel.OFF)
    #     # this should produce no output when logging is off
    #     self.test_intermodule_object_exchange()
    #     elsa.Logger.flush()
    #     with open(logfile_name) as log_file:
    #         self.assertTrue(len(log_file.readline()) == 0)
    #
    #     elsa.Logger.setLevel(elsa.LogLevel.TRACE)
    #     # this should produce some output when using a low logging level
    #     self.test_intermodule_object_exchange()
    #     elsa.Logger.flush()
    #     with open(logfile_name) as log_file:
    #         self.assertTrue(len(log_file.readline()) > 0)


if __name__ == "main":
    unittest.main()
