#include <memory>
#include <optional>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>

#include "LineSearchMethod.h"
#include "FixedStepSize.h"
#include "SteepestDescentStepLS.h"
#include "ArmijoCondition.h"
#include "BarzilaiBorwein.h"
#include "GoldsteinCondition.h"
#include "StrongWolfeCondition.h"

#include "bind_common.h"
#include "hints/line_search_hints.cpp"

namespace py = pybind11;

template <class data_t>
void add_definitions_line_search_method(
    py::class_<elsa::LineSearchMethod<data_t>, elsa::Cloneable<elsa::LineSearchMethod<data_t>>>
        line_search_method)
{
}

namespace detail
{
    template <class data_t>
    void add_line_search_method_cloneable(py::module& m, const char* name)
    {
        using LineSearchMethod = elsa::LineSearchMethod<data_t>;
        using Cloneable = elsa::Cloneable<LineSearchMethod>;

        py::class_<Cloneable> cloneable(m, name);
        cloneable
            .def("__ne__",
                 py::overload_cast<const LineSearchMethod&>(&Cloneable::operator!=, py::const_),
                 py::arg("other"))
            .def("__eq__",
                 py::overload_cast<const LineSearchMethod&>(&Cloneable::operator==, py::const_),
                 py::arg("other"))
            .def("clone", py::overload_cast<>(&Cloneable::clone, py::const_));
    }

    template <class data_t>
    void add_line_search_method(py::module& m, const char* name)
    {
        using LineSearchMethod = elsa::LineSearchMethod<data_t>;
        using Cloneable = elsa::Cloneable<LineSearchMethod>;
        using Problem = elsa::Functional<data_t>;

        py::class_<LineSearchMethod, Cloneable> line_search_method(m, name);

        line_search_method.def("solve", &LineSearchMethod::solve, py::arg("xi"), py::arg("di"),
                               py::return_value_policy::move);
    }
} // namespace detail

void add_line_search_method(py::module& m)
{
    detail::add_line_search_method_cloneable<float>(m, "CloneableLineSearchMethodf");
    detail::add_line_search_method_cloneable<double>(m, "CloneableLineSearchMethodd");

    detail::add_line_search_method<float>(m, "LineSearchMethodf");
    detail::add_line_search_method<double>(m, "LineSearchMethodd");

    m.attr("LineSearchMethod") = m.attr("LineSearchMethodf");
}

namespace detail
{

    template <class data_t>
    void add_fixed_step_size(py::module& m, const char* name)
    {
        using LineSearchMethod = elsa::LineSearchMethod<data_t>;
        using Problem = elsa::Functional<data_t>;

        py::class_<elsa::FixedStepSize<data_t>, LineSearchMethod> line_search_method(m, name);
        line_search_method.def(py::init<const Problem&, data_t>(), py::arg("problem"),
                               py::arg("stepSize") = 1);
    }

    template <class data_t>
    void add_steepest_descent_step_ls(py::module& m, const char* name)
    {
        using LineSearchMethod = elsa::LineSearchMethod<data_t>;

        py::class_<elsa::SteepestDescentStepLS<data_t>, LineSearchMethod> line_search_method(m,
                                                                                             name);
        line_search_method.def(py::init<const elsa::LeastSquares<data_t>&>(), py::arg("problem"));
    }

    template <class data_t>
    void add_armijo_condition(py::module& m, const char* name)
    {
        using LineSearchMethod = elsa::LineSearchMethod<data_t>;
        using Problem = elsa::Functional<data_t>;

        py::class_<elsa::ArmijoCondition<data_t>, LineSearchMethod> line_search_method(m, name);
        line_search_method.def(py::init<const Problem&, data_t, data_t, data_t, elsa::index_t>(),
                               py::arg("problem"), py::arg("amax") = 2, py::arg("c") = 0.1,
                               py::arg("rho") = 0.5, py::arg("max_iterations") = 10);
    }

    template <class data_t>
    void add_barzilai_borwein(py::module& m, const char* name)
    {
        using LineSearchMethod = elsa::LineSearchMethod<data_t>;
        using Problem = elsa::Functional<data_t>;

        py::class_<elsa::BarzilaiBorwein<data_t>, LineSearchMethod> line_search_method(m, name);
        line_search_method.def(
            py::init<const Problem&, uint32_t, data_t, data_t, data_t, data_t, elsa::index_t>(),
            py::arg("problem"), py::arg("m") = 10, py::arg("gamma") = 1e-4, py::arg("sigma1") = 0.1,
            py::arg("sigma2") = 0.5, py::arg("epsilon") = 1e-10, py::arg("max_iterations") = 10);
    }

    template <class data_t>
    void add_goldstein_condition(py::module& m, const char* name)
    {
        using LineSearchMethod = elsa::LineSearchMethod<data_t>;
        using Problem = elsa::Functional<data_t>;

        py::class_<elsa::GoldsteinCondition<data_t>, LineSearchMethod> line_search_method(m, name);
        line_search_method.def(py::init<const Problem&, data_t, data_t, elsa::index_t>(),
                               py::arg("problem"), py::arg("amax") = 10, py::arg("c") = 0.25,
                               py::arg("max_iterations") = 10);
    }

    template <class data_t>
    void add_strong_wolfe_condition(py::module& m, const char* name)
    {
        using LineSearchMethod = elsa::LineSearchMethod<data_t>;
        using Problem = elsa::Functional<data_t>;

        py::class_<elsa::StrongWolfeCondition<data_t>, LineSearchMethod> line_search_method(m,
                                                                                            name);
        line_search_method.def(py::init<const Problem&, data_t, data_t, data_t, elsa::index_t>(),
                               py::arg("problem"), py::arg("amax") = 10, py::arg("c1") = 1e-4,
                               py::arg("c2") = 0.9, py::arg("max_iterations") = 10);
    }
} // namespace detail

void add_line_search_methods(py::module& m)
{
    detail::add_fixed_step_size<float>(m, "FixedStepSizef");
    detail::add_fixed_step_size<double>(m, "FixedStepSized");

    m.attr("FixedStepSize") = m.attr("FixedStepSizef");

    detail::add_steepest_descent_step_ls<float>(m, "SteepestDescentStepLSf");
    detail::add_steepest_descent_step_ls<double>(m, "SteepestDescentStepLSd");

    m.attr("SteepestDescentStepLS") = m.attr("SteepestDescentStepLSf");

    detail::add_armijo_condition<float>(m, "ArmijoConditionf");
    detail::add_armijo_condition<double>(m, "ArmijoConditiond");

    m.attr("ArmijoCondition") = m.attr("ArmijoConditionf");

    detail::add_barzilai_borwein<float>(m, "BarzilaiBorweinf");
    detail::add_barzilai_borwein<double>(m, "BarzilaiBorweind");

    m.attr("BarzilaiBorwein") = m.attr("BarzilaiBorweinf");

    detail::add_goldstein_condition<float>(m, "GoldsteinConditionf");
    detail::add_goldstein_condition<double>(m, "GoldsteinConditiond");

    m.attr("GoldsteinCondition") = m.attr("GoldsteinConditionf");

    detail::add_strong_wolfe_condition<float>(m, "StrongWolfeConditionf");
    detail::add_strong_wolfe_condition<double>(m, "StrongWolfeConditiond");

    m.attr("StrongWolfeCondition") = m.attr("StrongWolfeConditionf");
}

void add_definitions_pyelsa_line_search(py::module& m)
{
    add_line_search_method(m);
    add_line_search_methods(m);

    elsa::LineSearchHints::addCustomFunctions(m);
}

PYBIND11_MODULE(pyelsa_line_search, m)
{
    add_definitions_pyelsa_line_search(m);
}
