*********
elsa core
*********

.. contents:: Table of Contents

DataContainer
=============

.. doxygenclass:: elsa::DataContainer
   :project: elsa

Transformations
---------------

.. doxygenfunction:: exp(const DataContainer<data_t> &dc)
   :project: elsa

.. doxygenfunction:: log(const DataContainer<data_t> &dc)
   :project: elsa

.. doxygenfunction:: square(const DataContainer<data_t> &dc)
   :project: elsa

.. doxygenfunction:: sqrt(const DataContainer<data_t> &dc)
   :project: elsa

.. doxygenfunction:: bessel_log_0(const DataContainer<data_t> &dc)
   :project: elsa

.. doxygenfunction:: bessel_1_0(const DataContainer<data_t> &dc)
   :project: elsa

.. doxygenfunction:: minimum(const DataContainer<data_t> &dc, SelfType_t<data_t> scalar)
   :project: elsa

.. doxygenfunction:: maximum(const DataContainer<data_t> &dc, SelfType_t<data_t> scalar)
   :project: elsa

.. doxygenfunction:: cwiseAbs(const DataContainer<data_t> &dc)
   :project: elsa

.. doxygenfunction:: cwiseMin(const DataContainer<xdata_t> &lhs, const DataContainer<ydata_t> &rhs)
   :project: elsa

.. doxygenfunction:: cwiseMax(const DataContainer<xdata_t> &lhs, const DataContainer<ydata_t> &rhs)
   :project: elsa

.. doxygenfunction:: sign(const DataContainer<data_t> &dc)
   :project: elsa

.. doxygenfunction:: real(const DataContainer<data_t> &dc)
   :project: elsa

.. doxygenfunction:: imag(const DataContainer<data_t> &dc)
   :project: elsa

.. doxygenfunction:: clip(const DataContainer<data_t> &dc, data_t min, data_t max)
   :project: elsa

.. doxygenfunction:: asComplex(const DataContainer<data_t> &dc)
   :project: elsa

Creation
--------

.. doxygenfunction:: zeros
   :project: elsa

.. doxygenfunction:: zeroslike
   :project: elsa

.. doxygenfunction:: ones
   :project: elsa

.. doxygenfunction:: oneslike
   :project: elsa

.. doxygenfunction:: full
   :project: elsa

.. doxygenfunction:: fulllike
   :project: elsa

.. doxygenfunction:: empty(const DataDescriptor &desc)
   :project: elsa

.. doxygenfunction:: emptylike
   :project: elsa

Others
------

.. doxygenfunction:: lincomb(SelfType_t<data_t> a, const DataContainer<data_t> &x, SelfType_t<data_t> b, const DataContainer<data_t> &y)
   :project: elsa

.. doxygenfunction:: lincomb(SelfType_t<data_t> a, const DataContainer<data_t> &x, SelfType_t<data_t> b, const DataContainer<data_t> &y, DataContainer<data_t> &out)
   :project: elsa

.. doxygenfunction:: materialize
   :project: elsa

.. doxygenfunction:: concatenate
   :project: elsa

.. doxygenfunction:: fftShift2D
   :project: elsa

.. doxygenfunction:: ifftShift2D
   :project: elsa


LinearOperator
==============

.. doxygenclass:: elsa::LinearOperator


Descriptors
===========

Descriptors are storing metda data for the DataContainer. They give meaning
to the actual data signal. Whether or not the data is the volume, measurements
or some blocked data format depends on the specific type of descriptor.

DataDescriptor
--------------

.. doxygenclass:: elsa::DataDescriptor

VolumeDescriptor
--------------

.. doxygenclass:: elsa::VolumeDescriptor

DetectorDescriptor
------------------

.. doxygenclass:: elsa::DetectorDescriptor

.. doxygenclass:: elsa::PlanarDetectorDescriptor

.. doxygenclass:: elsa::CurvedDetectorDescriptor

BlockDescriptor
---------------

.. doxygenclass:: elsa::BlockDescriptor

IdenticalBlocksDescriptor
-------------------------

.. doxygenclass:: elsa::IdenticalBlocksDescriptor

PartitionDescriptor
-------------------

.. doxygenclass:: elsa::PartitionDescriptor

RandomBlocksDescriptor
----------------------

.. doxygenclass:: elsa::RandomBlocksDescriptor


Core Type Declarations
======================

.. doxygenfile:: elsaDefines.h

Implementation Details
======================

Cloneable
---------

.. doxygenclass:: elsa::Cloneable

Utilities
=========

.. doxygenclass:: elsa::Badge
.. doxygenfile:: Statistics.hpp
